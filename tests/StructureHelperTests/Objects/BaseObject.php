<?php
namespace  Tests\StructureHelperTests\Objects;

class BaseObject
{
    /** @var string */
    public $stringValue;

    /** @var string[] */
    public $stringArray;

    /** @var bool */
    public $boolValue;

    /** @var float */
    public $doubleValue;

}