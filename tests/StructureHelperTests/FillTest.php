<?php

namespace  Tests\StructureHelperTests;

use ClientInterface\Base\PhpDocReader\AnnotationException;
use PHPUnit\Framework\TestCase;
use ClientInterface\Base\StructureHelper;
use Tests\StructureHelperTests\Objects\BaseObject;
use Tests\StructureHelperTests\Objects\ObjectWithObjects;
use LogicException;
use InvalidArgumentException;

class FillTest extends TestCase
{

    /**
     * @dataProvider successDataProvider
     *
     * @throws AnnotationException
     * @throws \ReflectionException
     */
    public function testSuccess($rawData)
    {
        $object = new ObjectWithObjects();

        StructureHelper::fill($object, $rawData);

        $arrayByObject = $this->toArray($object);

        $this->assertEquals($rawData, $arrayByObject);
    }

    /**
     * @dataProvider exceptionDataProvider
     *
     * @throws AnnotationException
     * @throws \ReflectionException
     */
    public function testExceptions($object, $rawData, $exceptionClass)
    {
        $this->expectException($exceptionClass);
        StructureHelper::fill($object, $rawData);
    }

    /**
     * @dataProvider ignoreExceptionDataProvider
     *
     * @throws AnnotationException
     * @throws \ReflectionException
     */
    public function testIgnoreExceptions($object, $rawData)
    {
        StructureHelper::fill($object, $rawData, false);
        $arrayByObject = $this->toArray($object);
        foreach ($arrayByObject as $key => $value) {
            $this->assertEquals($arrayByObject[$key], $rawData[$key] ?? null);
        }
        $this->assertNotEquals($arrayByObject,$rawData);
    }

    public function ignoreExceptionDataProvider()
    {
        return [
            [
                new BaseObject(),
                $this->makeRawData(),
            ]
        ];
    }

    public function exceptionDataProvider()
    {
        return [
            'test_different_keys' => [
                new BaseObject(),
                $this->makeRawData(),
                LogicException::class,
            ],
            'test_not_object' => [
                [],
                $this->makeRawData(),
                InvalidArgumentException::class,
            ]
        ];
    }

    public function successDataProvider()
    {
        return [
            [
                $this->makeRawData(),
            ]
        ];
    }

    private function makeRawData(): array
    {
        $data = [
            'stringValue' => 'string type',
            'stringArray' => [
                'first value',
                'second value'
            ],
            'boolValue' => true,
            'doubleValue' => 7.1,
        ];
        $data['objectValue'] = $data;

        return $data;
    }

    /**
     * @param ObjectWithObjects|BaseObject $object
     * @return array
     */
    private function toArray($object): array
    {
        return json_decode(json_encode($object), true);
    }
}