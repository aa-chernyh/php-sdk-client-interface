# Client interface
    Основа для создания SDK. Предлагает базовые правила для создания SDK и содержит метод fill, мапящий поля, пришедшие с ресурса в структуры SDK.
    Компоненты
        ClientInterface
            https://gitlab.fast-system.ru/ast/client-interface оределяет интерфейс компонентов sdk
            ClientInterface\Base\StructureHelper использует тип поля из phpdoc для заполениея объекта данными
            ClientInterface\Base\Validate позволяет описывать правила валидации объекта через метод rules()
    Общее
        В комментариях к полям или get|set методам указывать описание поля из документации и тип, если не указан в сигнатуре 
    Клиент
        Реализует ClientInterface\Client
        и getLastRequest() и getLastResponse() возвращаю запрос и ответ в формате api в виде массива
        Если используется формат на базе xml, релизовать методы getLastRequestXml() и getLastResponseXml() - отдают текст xml переданный, в api
        В комментариях к методам доступа к API указываются типы параметров и результат, а так же описание метода из документации
    DTO
        Если используется в запросе, то используется трейт ClientInterface\Base\Validate и указываются правила валидации
    Запросы
        Релизуют интерфейс ClientInterface\Request
        Используется трейт ClientInterface\Base\Validate и указываются правила валидации
    Ответы
        Релизуют интерфейс ClientInterface\Response


# Технические детали
Запуск тестов
```
vendor/bin/phpunit 
```
Запуск конкретного раздела тестов
```
vendor/bin/phpunit --testsuite fill-tests --debug
```
