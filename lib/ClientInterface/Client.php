<?php

namespace ClientInterface;

/**
 * Интерфейс клиента api
 * Выполняет запросы к сервисам и формирует объекты ответов из ответа сервиса
 * @package ClientInterface
 */
interface Client
{
    /**
     * Получить данные последнего запроса в виде массива
     * @return array
     */
    public function getLastRequest(): array;

    /**
     * Получить данные последнего ответа в виде массива
     * @return array
     */
    public function getLastResponse(): array;
}