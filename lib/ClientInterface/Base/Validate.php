<?php

namespace ClientInterface\Base;


use Assert\AssertionFailedException;
use ClientInterface\Exception\ValidationException;

trait Validate
{
    private $errors = [];


    public function getFirstErrors(): array
    {
        $errors = $this->getErrors();
        $firstErrors = [];

        foreach ($errors as $attributeName => $attributeErrors) {
            if (!empty($attributeErrors)) {
                $firstError = current($attributeErrors);
                if (is_array($firstError)) {
                    $firstError = current($firstError);
                }
                $firstErrors[$attributeName] = $firstError;
            }
        }

        return $firstErrors;
    }

    public function getErrors($attribute = null): array
    {
        $errors = $this->errors;

        foreach ($this->getValidatableObjectAttributeNames() as $validatableObjectAttributeName) {
            $nestedAttributeErrors = $this->getErrorsFromInnerValidatable($this->{$validatableObjectAttributeName});
            if (!empty($nestedAttributeErrors)) {
                foreach ($nestedAttributeErrors as $nestedAttributeName => $attributeError) {
                    if ($this->isMultipleAttribute($validatableObjectAttributeName)) {
                        foreach ($attributeError as $key => $item) {
                            $errors[$validatableObjectAttributeName . '.' . $nestedAttributeName . '.' . $key] = $item;
                        }
                    } else {
                        $errors[$validatableObjectAttributeName . '.' . $nestedAttributeName] = $attributeError;
                    }
                }
            }
        }

        return empty($attribute) ? $errors : ($errors[$attribute] ?? []);
    }

    protected function mergeValidationRules(array $oneRules, array $otherRules): array
    {
        foreach ($otherRules as $key => $rules) {
            if (!isset($oneRules[$key])) {
                $oneRules[$key] = [0 => []];
            }
            $oneRules[$key][0] = array_merge($oneRules[$key][0], $rules[0]);
            $oneRules[$key][1] = $rules[1];
        }

        return $oneRules;
    }

    private function isMultipleAttribute($attributeName)
    {
        return isset($this->{$attributeName}) && is_array($this->{$attributeName});
    }

    /**
     * @return bool
     * @throws ValidationException
     */
    public function validate(): bool
    {
        $this->errors = [];

        foreach ($this->rules() as $ruleName => $rule) {
            if (!is_array($rule)) {
                throw new ValidationException('Правило валидации должно быть массивом');
            }

            if (!is_string($rule[0]) && !is_array($rule[0])) {
                throw new ValidationException('Первый элемент массива-правила должен быть именем атрибута или массивом имён');
            }
            $attributes = array_shift($rule);
            if (!is_array($attributes)) {
                $attributes = [$attributes];
            }

            if (empty($rule)) {
                throw new ValidationException('Необходимо указать хоть одну функцию-правило валидации');
            }

            foreach ($rule as $key => $attributesValidationRule) {
                if (!is_callable($attributesValidationRule)) {
                    throw new ValidationException('Правило проверки должно быть функцией');
                }

                foreach ($attributes as $attribute) {
                    try {
                        call_user_func($attributesValidationRule, $this->{$attribute}, $attribute);
                    } catch (AssertionFailedException $e) {
                        $this->addError($attribute, $e->getMessage());
                    }
                }
            }
        }

        $success = empty($this->errors);
        foreach ($this->getValidatableObjectAttributeNames() as $validatableObjectAttributeName) {
            $success = $this->validateInnerValidatable($this->{$validatableObjectAttributeName}) && $success;
        }

        return $success;
    }

    /**
     * Массив правил валидации
     * Работает через перехват исключений AssertionFailedException
     * ```php
     * [
     *      ['attribute', function($value, $attribute) {
     *          Assert::that($value)->notEmpty()->integer();
     *      }],
     *      'string' => [['attribute2', 'attribute3'], function($value, $attribute) {
     *          Assert::that($value)->notNull();
     *      }],
     * ]
     * ```
     *
     * @return array
     * @see AssertionFailedException
     */
    abstract protected function rules(): array;

    private function getValidatableObjectAttributeNames(): array
    {
        $validatableAttributes = [];
        $attributes = get_object_vars($this);

        foreach ($attributes as $attributeName => $attributeValue) {
            if ($this->checkIsValidatable($attributeValue)) {
                $validatableAttributes[] = $attributeName;
            }
        }

        return $validatableAttributes;
    }

    protected function addError($attribute, $error)
    {
        if (empty($error)) {
            return;
        }
        if (!isset($this->errors[$attribute])) {
            $this->errors[$attribute] = [];
        }
        $this->errors[$attribute][] = $error;
    }

    /**
     * @param $object
     * @return bool
     */
    private function checkIsValidatable($object): bool
    {
        if (is_array($object)) {
            return $this->checkIsValidatable(current($object));
        }
        return is_object($object) && method_exists($object, 'validate');
    }

    /**
     * @param Validate|Validate[] $object
     * @return array
     */
    private function getErrorsFromInnerValidatable($object)
    {
        $errors = [];
        if ($this->checkIsValidatable($object)) {
            if (is_array($object)) {
                foreach ($object as $key => $item) {
                    if ($innerErrors = $this->getErrorsFromInnerValidatable($item)) {
                        $errors[] = $innerErrors;
                    }
                }
            } else {
                $errors = $object->getErrors();
            }
        }

        return $errors;
    }

    /**
     * @param Validate $object
     * @return bool
     * @throws ValidationException
     */
    private function validateInnerValidatable($object)
    {
        $success = true;
        if ($this->checkIsValidatable($object)) {
            if (is_array($object)) {
                foreach ($object as $key => $item) {
                    $success = $this->validateInnerValidatable($item) && $success;
                }
            } else {
                $success = $object->validate() && $success;
            }
        }

        return $success;
    }
}