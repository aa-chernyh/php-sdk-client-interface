<?php

declare(strict_types=1);

namespace ClientInterface\Base;


use ClientInterface\Base\PhpDocReader\AnnotationException;
use ClientInterface\Base\PhpDocReader\PhpDocReader;
use ClientInterface\Base\PhpDocReader\PropertyMeta;
use InvalidArgumentException;
use LogicException;
use ReflectionClass;
use ReflectionException;
use ReflectionProperty;

class StructureHelper
{
    /**
     * @param object $object
     * @return array
     */
    public static function toArray($object)
    {
        return json_decode(json_encode($object), true);
    }

    /**
     * @param object $object
     * @param array $rawData
     * @param bool $throwsOnDiff
     * @return void
     * @throws AnnotationException
     * @throws ReflectionException
     */
    public static function fill($object, array $rawData, $throwsOnDiff = true): void
    {
        if (!is_object($object)) {
            throw new InvalidArgumentException('First attribute must be an object, got ' . gettype($object));
        }

        $reflectionClass = new ReflectionClass($object);

        $publicNames = [];

        foreach ($reflectionClass->getProperties(ReflectionProperty::IS_PUBLIC) as $property) {
            if (!$property->isStatic()) {
                $publicNames[] = $property->getName();
            }
        }

        $diff = array_diff(array_keys($rawData), $publicNames);

        if ($diff && $throwsOnDiff) {
            $diffDetails = [];
            foreach ($diff as $diffKey) {
                $diffDetails[$diffKey] = $rawData[$diffKey];
            }
            throw new LogicException(
                'Raw data contains fields, missed or hidden in target class: ' . json_encode($diffDetails)
            );
        }

        $reader = new PhpDocReader();

        foreach ($publicNames as $propertyName) {
            $propertyMeta = $reader->getPropertyClass(new ReflectionProperty($object, $propertyName));

            if ($propertyMeta->isPrimitive) {
                if (isset($rawData[$propertyName])) {
                    $object->{$propertyName} = $propertyMeta->isArray ? $rawData[$propertyName] : self::convertToType($rawData[$propertyName], $propertyMeta->type);
                } else {
                    $object->{$propertyName} = $propertyMeta->isArray ? [] : null;
                }
            } else if ($propertyMeta->class) {
                if ($propertyMeta->isArray) {
                    $object->{$propertyName} = [];

                    $rawDataArrayValue = $rawData[$propertyName] ?? [];
                    $isArrayOfArray = !empty($rawDataArrayValue) && is_array(current($rawDataArrayValue));

                    if ($isArrayOfArray) {
                        foreach ($rawDataArrayValue as $value) {
                            $dataInstance = self::instantiateProperty($value, $propertyMeta, $throwsOnDiff);
                            $object->{$propertyName}[] = $dataInstance;
                        }
                    } else if (!empty($rawDataArrayValue)) {
                        $dataInstance = self::instantiateProperty($rawDataArrayValue, $propertyMeta, $throwsOnDiff);
                        $object->{$propertyName}[] = $dataInstance;
                    }
                } else {
                    $value = $rawData[$propertyName] ?? null;

                    $dataInstance = self::instantiateProperty($value, $propertyMeta, $throwsOnDiff);

                    $object->{$propertyName} = $dataInstance;
                }
            }
        }
    }

    private static function convertToType($value, string $type)
    {
        if (is_null($value)) {
            return null;
        }
        if (is_array($value) && $type != 'array') {
            return null;
        }
        if ($type != 'mixed') {
            settype($value, $type);
        }
        return $value;
    }

    /**
     * @param $value
     * @param PropertyMeta $propertyMeta
     * @param bool $throwsOnDiff
     * @return mixed
     * @throws AnnotationException
     * @throws ReflectionException
     */
    private static function instantiateProperty($value, PropertyMeta $propertyMeta, $throwsOnDiff = true)
    {
        if ($value === null && $propertyMeta->isNullable) {
            return null;
        }
        if (is_array($value)) {
            $dataInstance = new $propertyMeta->class();

            static::fill($dataInstance, $value, $throwsOnDiff);
        } else {
            $dataInstance = new $propertyMeta->class($value);
        }
        return $dataInstance;
    }
}