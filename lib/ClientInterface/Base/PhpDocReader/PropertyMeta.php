<?php

declare(strict_types=1);

namespace ClientInterface\Base\PhpDocReader;

class PropertyMeta
{
    /**
     * @var string|null
     */
    public $type;

    /**
     * @var string|null
     */
    public $class;

    /**
     * @var bool|null
     */
    public $isPrimitive = false;

    /**
     * @var bool|null
     */
    public $isArray = false;

    /**
     * @var bool|null
     */
    public $isNullable = false;
}
