<?php

declare(strict_types=1);

namespace ClientInterface\Base\PhpDocReader;

use Exception;


class AnnotationException extends Exception
{
}
