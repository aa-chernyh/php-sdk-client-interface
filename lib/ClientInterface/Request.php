<?php

namespace ClientInterface;

use ClientInterface\Exception\ValidationException;

/**
 * Интерфейс запроса к api
 * Представляет собой запрос к сервису
 * Позволяет производить предварительную валидацию данных на стороне клиента
 * @package ClientInterface
 */
interface Request
{
    /**
     * Проверка валидности данных запроса
     * @return bool
     * @throws ValidationException
     */
    public function validate(): bool ;

    /**
     * Получить массив первых ошибок для атрибутов
     * @return array
     */
    public function getFirstErrors(): array ;

    /**
     * Получить массив всех ошибок для атрибута или всех атрибутов
     * @param null $attribute
     * @return array
     */
    public function getErrors($attribute = null): array ;

    /**
     * Представление запроса в виде массива
     * @return array
     */
    public function toArray(): array ;
}