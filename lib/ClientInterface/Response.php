<?php

namespace ClientInterface;

/**
 * Интерфейс ответа сервиса
 * Представляет данные и логику работы с ответом сервиса api
 * Позволяет установить успешность ответа и получить список ошибок от сервиса
 * @package ClientInterface
 */
interface Response
{
    /**
     * Является ли результат успешным
     * @return bool
     */
    public function isSuccess(): bool;

    /**
     * Получить список ошибок из ответа
     * @return array
     */
    public function getErrors(): array;
}